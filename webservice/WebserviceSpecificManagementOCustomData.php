<?php
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */
class WebserviceSpecificManagementOCustomData implements WebserviceSpecificManagementInterface
{
    private WebserviceOutputBuilderCore $objOutput;
    private WebserviceRequestCore $wsObject;
    private $output;

    public function manage()
    {
        if (!isset($this->wsObject->urlFragments['address'])) {
            throw new WebserviceException('You have to set the \'address\' parameters to get a result', [100, 400]);
        }
        $data = [];
        $data['empty'] = new Customer();

        $this->_resourceConfiguration = $data['empty']->getWebserviceParameters();

        if (!$this->wsObject->setFieldsToDisplay()) {
            return false;
        }

        $results = array_merge($data, array_map(function ($customer) { return new Customer($customer['id_customer']); }, Customer::getCustomersByEmail($this->wsObject->urlFragments['address'])));

        $this->output .= $this->objOutput->getContent($results, null, $this->wsObject->fieldsToDisplay, $this->wsObject->depth, WebserviceOutputBuilder::VIEW_LIST, false);
        return true;
    }

    public function getContent()
    {
        return $this->objOutput->getObjectRender()->overrideContent($this->output);
    }


    /**
     * @param WebserviceOutputBuilderCore $obj
     *
     * @return WebserviceSpecificManagementInterface
     */
    public function setObjectOutput(WebserviceOutputBuilderCore $obj): WebserviceSpecificManagementInterface
    {
        $this->objOutput = $obj;

        return $this;
    }

    public function setWsObject(WebserviceRequestCore $obj): WebserviceSpecificManagementOCustomData
    {
        $this->wsObject = $obj;

        return $this;
    }

    public function getWsObject(): WebserviceRequestCore
    {
        return $this->wsObject;
    }

    public function getObjectOutput(): WebserviceOutputBuilderCore
    {
        return $this->objOutput;
    }

    public function setUrlSegment($segments): WebserviceSpecificManagementOCustomData
    {
        $this->urlSegment = $segments;

        return $this;
    }

    public function getUrlSegment()
    {
        return $this->urlSegment;
    }
}
