<?php
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

include_once _PS_MODULE_DIR_ . 'openscop_api/models/OData.php';
include_once _PS_MODULE_DIR_ . 'openscop_api/webservice/WebserviceSpecificManagementOCustomData.php';

class Openscop_Api extends Module
{
    const HOOKS = [
        'addWebserviceResources'
    ];

    public function __construct()
    {
        $this->name = 'openscop_api';
        $this->version = '1.0.0';
        $this->author = 'Openscop';
        $this->need_instance = 0;

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->trans('Openscop api', [], 'Modules.Openscop.Api.Admin');
        $this->description = $this->trans('test api with module ressource.', [], 'Modules.Openscop.Api.Admin');

        $this->ps_versions_compliancy = ['min' => '1.7.8.0', 'max' => '1.7.8.6'];

    }

    public function isUsingNewTranslationSystem()
    {
        return true;
    }

    public function install()
    {
        return parent::install() &&
            $this->registerHook(self::HOOKS) &&
            $this->createDb();
    }

    public function uninstall()
    {
        Configuration::deleteByName('OPENSCOP_API_ENABLE');

        return parent::uninstall();
    }

    public function createDb() {
        $db = Db::getInstance();
        $done = false;
        $done = $db->execute('create table if not exists '. _DB_PREFIX_ .'odata
(
    id_odata int,
    name     varchar(255) not null,
    date_add datetime     not null,
    date_upd datetime     not null
)');
        $done = $db->execute('create unique index odata_id_odata_uindex on '. _DB_PREFIX_ .'odata (id_odata)');
        $done = $db->execute('alter table '. _DB_PREFIX_ .'odata add constraint odata_pk primary key (id_odata)');
        $done = $db->execute('alter table '. _DB_PREFIX_ .'odata modify id_odata int auto_increment');
        return $done;
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitStoreConf')) {
            Configuration::updateValue('OPENSCOP_API_ENABLE', Tools::getValue('OPENSCOP_API_ENABLE', false));

            return $this->displayConfirmation($this->trans('The settings have been updated.', [], 'Admin.Notifications.Success'));
        }

        return '';
    }

    public function getContent()
    {
        return $this->postProcess() . $this->renderForm();
    }

    public function renderForm()
    {
        $fields_form = [
            'form' => [
                'legend' => [
                    'title' => $this->trans('Settings', [], 'Admin.Global'),
                    'icon' => 'icon-cogs'
                ],
                'input' => [
                    [
                        'type' => 'switch',
                        'label' => $this->trans('Enable api', [], 'Modules.Openscop.Api.Admin'),
                        'name' => 'OPENSCOP_API_ENABLE',
                        'desc' => $this->trans('Activate the api for the module ressources.', [], 'Modules.Openscop.Api.Admin'),
                        'values' => [
                            [
                                'id' => 'OPENSCOP_API_ENABLE_on',
                                'value' => true,
                                'label' => $this->trans('yes', [], 'Admin.Openscop.Api.Actions')
                            ],
                            [
                                'id' => 'OPENSCOP_API_ENABLE_off',
                                'value' => false,
                                'label' => $this->trans('no', [], 'Admin.Openscop.Api.Actions')
                            ]
                        ],
                    ],
                ],
                'submit' => [
                    'title' => $this->trans('Save', [], 'Admin.Actions')
                ]
            ],
        ];

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->module = $this;
        $helper->submit_action = 'submitStoreConf';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = [
            'uri' => $this->getPathUri(),
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        ];

        return $helper->generateForm([$fields_form]);
    }

    public function getConfigFieldsValues()
    {
        $fields = [];
        $fields['OPENSCOP_API_ENABLE'] = Tools::getValue('OPENSCOP_API_ENABLE', Configuration::get('OPENSCOP_API_ENABLE'));
        return $fields;
    }

    public function hookAddWebserviceResources(): array
    {
        if (Configuration::get('OPENSCOP_API_ENABLE')) {
            return [
                'odata' => [
                    'description' => 'data openscop',
                    'class' => 'OData',
                    'forbidden_method' => ['PUT', 'POST', 'DELETE']
                ],
                'ocustomdata' => [
                    'description' => 'data formatter par openscop',
                    'specific_management' => true,
                    'forbidden_method' => ['PUT', 'POST', 'DELETE']
                ],
            ];
        }
        return [];
    }
}
